/* 
/*     */ 
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.simple.JSONValue;
public class POST2GCM
/*     */ {
/*     */   String apiKey;
/*     */ 
/*     */   public POST2GCM()
/*     */   {
/*  21 */     this.apiKey = "AIzaSyCJpf1nzkBUyyN7Y2MrlTrkSHjr0i2gk9I";
/*     */   }
/*     */ 
/*     */   public static String post(String apiKey, Content content, List<String> regId) throws JsonGenerationException {
/*  25 */     String strResult = "";
/*     */     try
/*     */     {
/*  28 */       URL url = new URL("https://android.googleapis.com/gcm/send");
/*  29 */       Map payload = content.getData();
				//Map notification = data1;
/*  30 */       Map jsonRequest = new HashMap();
/*  31 */       jsonRequest.put("registration_ids", regId);
/*  32 */       jsonRequest.put("data", payload);
				//jsonRequest.put("notification", notification);
/*  33 */       String requestBody = JSONValue.toJSONString(jsonRequest);
/*  34 */       System.out.println(requestBody);
/*  35 */       HttpURLConnection conn = (HttpURLConnection)url.openConnection();
/*     */ 
/*  37 */       conn.setRequestMethod("POST");
/*     */ 
/*  39 */       conn.setRequestProperty("Content-Type", "application/json");
/*  40 */       conn.setRequestProperty("Authorization", "key=" + apiKey);
/*     */ 
/*  42 */       conn.setDoOutput(true);
/*     */ 
/*  44 */       ObjectMapper mapper = new ObjectMapper();
/*     */ 
/*  46 */       DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
/*  47 */       String req = mapper.writeValueAsString(content);
/*  48 */       System.out.println("Mapper::" + req);
/*  50 */       mapper.writeValue(wr, content);
/*  51 */       wr.flush();
/*  52 */       wr.close();
/*  53 */       int responseCode = conn.getResponseCode();
/*  54 */       System.out.println("Response Code : " + responseCode);
/*  55 */       BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
/*     */ 
/*  57 */       StringBuffer response = new StringBuffer();
/*  58 */       String inputLine = "";
/*  59 */       while ((inputLine = in.readLine()) != null)
/*     */       {
/*  62 */         response.append(inputLine);
/*     */       }
/*  64 */       in.close();
/*     */ 
/*  66 */       String res = Integer.toString(responseCode);
/*  67 */       if (res.equals("200")) {
/*  68 */         strResult = responseCode + "#" + response.toString();
/*  69 */         System.out.println(response.toString());
/*     */       } else {
/*  71 */         strResult = responseCode + "#" + "Error";
/*     */       }
/*     */     }
/*     */     catch (MalformedURLException e)
/*     */     {
/*  77 */       e.printStackTrace();
/*     */     } catch (IOException e) {
/*  79 */       e.printStackTrace();
/*     */     }
/*  81 */     return strResult;
/*     */   }
/*     */ 
/*     */   protected HttpURLConnection post(String url, String body) throws IOException {
/*  85 */     return post(url, "application/x-www-form-urlencoded;charset=UTF-8", body);
/*     */   }
/*     */ 
/*     */   protected HttpURLConnection post(String url, String contentType, String body) throws IOException {
/*  89 */     if ((url == null) || (body == null)) {
/*  90 */       throw new IllegalArgumentException("arguments cannot be null");
/*     */     }
/*  92 */     if (!url.startsWith("https://"));
/*  97 */     byte[] bytes = body.getBytes();
/*  98 */     HttpURLConnection conn = getConnection(url);
/*  99 */     conn.setDoOutput(true);
/* 100 */     conn.setUseCaches(false);
/* 101 */     conn.setFixedLengthStreamingMode(bytes.length);
/* 102 */     conn.setRequestMethod("POST");
/* 103 */     conn.setRequestProperty("Content-Type", contentType);
/* 104 */     conn.setRequestProperty("Authorization", "key=" + this.apiKey);
/* 105 */     OutputStream out = conn.getOutputStream();
/*     */     try {
/* 107 */       out.write(bytes);
/*     */     } finally {
/* 109 */       close(out);
/*     */     }
/* 111 */     return conn;
/*     */   }
/*     */   protected HttpURLConnection getConnection(String url) throws IOException {
/* 114 */     HttpURLConnection conn = (HttpURLConnection)new URL(url).openConnection();
/* 115 */     return conn;
/*     */   }
/*     */   private static void close(Closeable closeable) {
/* 118 */     if (closeable == null) return;
/*     */     try {
/* 120 */       closeable.close();
/*     */     }
/*     */     catch (IOException localIOException)
/*     */     {
/*     */     }
/*     */   }
private  Content createContent(String notification_code, String message, String title, String longDescription,String image, List regids)
/*     */   {
/*  42 */     Content c = new Content();
/*     */ 
/*  44 */     c.addListRegId(regids);
/*  45 */     c.createData(notification_code, message, title, longDescription,image);
/*     */ 
/*  52 */     return c;
/*     */   }
/*     */ 
public Bitmap getBitmapFromURL(String strURL) {
    try {
        URL url = new URL(strURL);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoInput(true);
        connection.connect();
        InputStream input = connection.getInputStream();
        Bitmap myBitmap = BitmapFactory.decodeStream(input);
        return myBitmap;
    } catch (IOException e) {
        e.printStackTrace();
        return null;
    }
}
public static void main(String arg[])
{

	POST2GCM objPOST2GCM=new POST2GCM();
	
	
	try
	{
		String largeIcon="http://fscomps.fotosearch.com/compc/CSP/CSP481/k4815511.jpg";
		//String largeIcon="https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcSu8UXNAE7IwoQQJl_DzNMa6uYGDY7ZbITxutRl3Ry2LPWRBHAV";
		/*Bitmap bmURL=objPOST2GCM.getBitmapFromURL(largeIcon);
		float multiplier= getImageFactor(getResources());
		bmURL=Bitmap.createScaledBitmap(bmURL, (int)(bmURL.getWidth()*multiplier), (int)(bmURL.getHeight()*multiplier), false);
		if(bmURL!=null){
		    mBuilder.setLargeIcon(bmURL);
		} */  
	ArrayList<String> x = new ArrayList<String>();
	//bsnl
	//x.add("APA91bGcicZ3aramqOQDzhLMW5YaNLtMKsIba9b5-8vzwU5w8SNoXCR8Nab_hZRePPi3JOlBCJfNNAlOQ_ZVrxBhCkpn6UudhkqaSgV6aSEdCwYIFMNSq2aow3bq1FOHyEY6PqUEMVma");
	
	//kshitiij 9041817138
	x.add("APA91bGaGmAHDZYzV6bS709dARlABgO9r9za-XODaMFH6cMKnAFkeqwO5EWY8rpECe_bjZJ1oskiq9QZ474gJsVZYJLtqsrmOu9lhSsxYuVRbJrdrpqQvjDSbO31h2-6PQn8EsdYrl0E");

	//jsp
	//x.add("APA91bEC9HhccVhj-MMFmDJ0Ljs-lROR6fqbvxFE4jsgMNcuJgI-inhWMz36E_M3yrBLcs-5pyW_N0jgyYY3VZzl0PEeDMRLlkShVicX6E1HtTGguuGe_c5xMA1cktpqmkqoUsuymQbg");
	//anuj
	//x.add("APA91bH3c0f_BJHE864LSVc66enH5gcGyMXWS-FeCEnckhLWKa6dvwzW_bWx89mWm5M8QAQ5-MfLeNaNWQK0uBHx-QxRtA6vrKB99YdyWG5JK_Z_o2zBM9jyBbSduSoGffdQgt3QUlPP");
	//gg
	//x.add("APA91bGu73P96P2RIufwWsgtoivdYdczk3P8SNB0dkkCcN6XC58_yztYjfYLe6Dd0nGEUDhgFSXH5pNKGPaROJEiDdV9tHrawWzpwUjtcFimLmWkH3G4E9raMdLYGP_nUg5BjwwZSE6S");
	//anil
	//x.add("APA91bGe6mZQLpU5wjV2GTZFlNVuSMuyjkey1kpTzzQiuzE_aXMmOFvW5kmbmvg5JjoM-0qkQUx6caEkDabYWhody8a5El96ERysDSsP8zXzZDUOxFcGLn-IqchBzL2lQn9n1y95MT8z");
    // sandeep sir
	//x.add("APA91bFEh6qNlOUq7QjL9xRXoUBPdh7BYquZxAsc-eNImlodZeCqsgnSHBmtN2xIVf4w9oW7EdrG0SRdPfP_4GfLKhALIhe9MVG7n8gQUfYQNYCXiJrW8Xeb3JD12z7yF26BeMqOlPfg");
	//Content content = objPOST2GCM.createContent("8", "Yaarri App", "Yaarri", "How r u", "https://www.yaarri.com/YaarriWebService/api/getFile/agent/image/1.jpg", x);
	//Content content = objPOST2GCM.createContent("8", "Yaarri App Hi Friend, Please update your profile.", "Hi Friend, Please update your profile.", "Your below details are inappropriate to be shown to other user on the app:\n\n1. Name\n2.About\n3. Location \n\n Please modify the content to find the more friend \n\nCheer\nTeam Yaarri",largeIcon, x);
	//String strResponse=objPOST2GCM.post(objPOST2GCM.apiKey,content,x);
	//System.out.println("strResponse : " + strResponse);
	
	Content  content = objPOST2GCM.createContent("7", "Yaarri App", "Yaarri2", "", "http://fscomps.fotosearch.com/compc/CSP/CSP481/k4815511.jpg", x);
    String strResponse=objPOST2GCM.post(objPOST2GCM.apiKey,content,x); 
    System.out.println("strResponse : " + strResponse);
	/*Thread.sleep(2*1000);
	content = objPOST2GCM.createContent("2", "Yaarri App", "Yaarri2", "How r u", "http://fscomps.fotosearch.com/compc/CSP/CSP481/k4815511.jpg", x);
    strResponse=objPOST2GCM.post(objPOST2GCM.apiKey,content,x);    
	System.out.println("strResponse : " + strResponse);
	Thread.sleep(2*1000);
	content = objPOST2GCM.createContent("3", "Yaarri App", "Yaarri3", "How r u", "http://fscomps.fotosearch.com/compc/CSP/CSP481/k4815511.jpg", x);
	strResponse=objPOST2GCM.post(objPOST2GCM.apiKey,content,x);
	System.out.println("strResponse : " + strResponse);
	Thread.sleep(2*1000);
	content = objPOST2GCM.createContent("4", "Yaarri App", "Yaarri4", "How r u", "http:/fscomps.fotosearch.com/compc/CSP/CSP481/k4815511.jpg", x);
	strResponse=objPOST2GCM.post(objPOST2GCM.apiKey,content,x);
	System.out.println("strResponse : " + strResponse);
	Thread.sleep(2*1000);
	content = objPOST2GCM.createContent("5", "Yaarri App", "Yaarri5", "How r u", "http://fscomps.fotosearch.com/compc/CSP/CSP481/k4815511.jpg", x);
	strResponse=objPOST2GCM.post(objPOST2GCM.apiKey,content,x);
	System.out.println("strResponse : " + strResponse);
	Thread.sleep(2*1000);
	content = objPOST2GCM.createContent("6", "Yaarri App", "Yaarri6", "How r u", "http://fscomps.fotosearch.com/compc/CSP/CSP481/k4815511.jpg", x);
	strResponse=objPOST2GCM.post(objPOST2GCM.apiKey,content,x);
	System.out.println("strResponse : " + strResponse);
	Thread.sleep(2*1000);
	content = objPOST2GCM.createContent("7", "Yaarri App", "Yaarri7", "How r u", "http://fscomps.fotosearch.com/compc/CSP/CSP481/k4815511.jpg", x);
	strResponse=objPOST2GCM.post(objPOST2GCM.apiKey,content,x);
	System.out.println("strResponse : " + strResponse);
	Thread.sleep(2*1000);
	content = objPOST2GCM.createContent("8", "Yaarri App", "Yaarri8", "How r u", "http://comps.canstockphoto.com/can-stock-photo_csp4815507.jpg", x);
    
	strResponse=objPOST2GCM.post(objPOST2GCM.apiKey,content,x);
	System.out.println("strResponse : " + strResponse);*/
	
	
	
	
	}
	catch(Exception ec)
	{ec.printStackTrace();}
	}
 }

/* Location:           C:\Users\ch-e01090\Desktop\RunAppPromo.jar
 * Qualified Name:     in.spicedigital.gcm.API.POST2GCM
 * JD-Core Version:    0.5.4
 */