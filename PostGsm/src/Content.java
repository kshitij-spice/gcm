/*   
/*    */ 
/*    */ import java.io.Serializable;
/*    */ import java.util.HashMap;
/*    */ import java.util.LinkedList;
/*    */ import java.util.List;
/*    */ import java.util.Map;
/*    */ import java.util.UUID;
/*    */ 
/*    */ public class Content
/*    */   implements Serializable
/*    */ {
/*    */   UUID uuid;
/*    */   private List<String> registration_ids;
/*    */   private Map<String, Object> data;
/*    */ 
/*    */   public Content()
/*    */   {
/* 21 */     this.uuid = UUID.randomUUID();
/*    */   }
/*    */ 
/*    */   public void addListRegId(List<String> regId) {
/* 25 */     if (this.registration_ids == null) {
/* 26 */       this.registration_ids = new LinkedList();
/*    */     }
/* 28 */     this.registration_ids = regId;
/*    */   }
/*    */ 
public void createData(String notification_code, String message, String title, String longDescription, String image)
 {
	    if (this.data == null)
	    {
	       this.data = new HashMap();
	    }
	     this.data.put("notification_code", notification_code);
	     this.data.put("title", title);
	     this.data.put("message", message); 
         this.data.put("userid", "");
         this.data.put("image", image);
         this.data.put("longDescription", longDescription); 
         //this.data.put("body", longDescription);
         
            // this.data.put("body", "Test");
            // this.data.put("icon", "small_icon");
            // this.data.put("largeIcon", image);
            // this.data.put("smallIcon", image);             
             //this.data.put("tickerText", message);
             
             
}
/*    */ 
/*    */   public List<String> getRegistration_ids()
/*    */   {
/* 44 */     return this.registration_ids;
/*    */   }
/*    */ 
/*    */   public void setRegistration_ids(List<String> registration_ids) {
/* 48 */     this.registration_ids = registration_ids;
/*    */   }
/*    */ 
/*    */   public Map<String, Object> getData() {
/* 52 */     return this.data;
/*    */   }
/*    */ 
/*    */   public void setData(Map<String, Object> data) {
/* 56 */     this.data = data;
/*    */   }
/*    */ }

/* Location:           C:\Users\ch-e01090\Desktop\RunAppPromo.jar
 * Qualified Name:     in.spicedigital.gcm.API.Content
 * JD-Core Version:    0.5.4
 */